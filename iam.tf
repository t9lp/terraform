resource "aws_iam_instance_profile" "bastion" {
  name = "${var.tag}-bastion-profile"
  role = "${aws_iam_role.bastion.name}"
}

resource "aws_iam_role" "bastion" {
  name = "${var.tag}-bastion-role"
  path = "/"
  assume_role_policy = "${file("assume.json")}"
}

resource "aws_iam_role_policy" "bastion" {
  name = "${var.tag}-bastion-policy"
  role = "${aws_iam_role.bastion.id}"
  policy = "${file("iam-bastion.json")}"
}

resource "aws_iam_user" "ro-user" {
  name = "ro-user"
  path = "/"
  force_destroy = true
}
resource "aws_iam_group" "ro-user" {
  name = "ro-user"
  path = "/"
}
resource "aws_iam_group_membership" "ro-user-membership" {
  name = "admin-user-membership"
  users = [
    "${aws_iam_user.ro-user.name}",
  ]
  group = "${aws_iam_group.ro-user.name}"
}
resource "aws_iam_group_policy_attachment" "ro-user" {
  group = "${aws_iam_group.ro-user.name}"
  policy_arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}
resource "aws_iam_user_login_profile" "ro-user" {
  user = "${aws_iam_user.ro-user.name}"
  pgp_key = "${file("terraform.pub")}"
  password_reset_required = false
}
output "name" {
  value = "${aws_iam_user.ro-user.name}"
  description = "IAM user's name."
}
output "password" {
  value = "${aws_iam_user_login_profile.ro-user.encrypted_password}"
  description = "IAM user's password."
}
