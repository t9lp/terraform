variable "region" {
  default = "eu-central-1"
}
variable "instance_type" {
  default = "t2.micro"
}
variable "key_name" {
  default = "aws-key"
}
variable "tag" {
  default = "terraform-test"
}
variable "cidr_block" {
  default = "172.17.0.0/16"
}
variable "cidr_half_block" {
  default = "172.17."
}
variable "aws-ami" {
  default = "ami-a058674b"
}
variable "bucket-name" {
  default = "s3-vbachinov-website-test"
}
locals {
  subnet_count = 3
  availability_zones = [
    "eu-central-1a",
    "eu-central-1b",
    "eu-central-1c"]
}
variable "mime_types" {
  default = {
    htm = "text/html"
    html = "text/html"
    css = "text/css"
    less = "plain/text"
    scss = "text/x-scss"
    js = "application/javascript"
    map = "application/javascript"
    json = "application/json"
    ttf = "font/ttf"
    woff = "font/woff"
    woff2 = "font/woff2"
    jpg = "image/jpg"
    eot = "application/vnd.ms-fontobject"
    otf = "application/font-otf"
    svg = "image/svg+xml"
    mp4 = "video/mp4"
  }
}