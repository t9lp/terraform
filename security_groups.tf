resource "aws_security_group" "http" {
  description = "Terraform Test HTTP SG"
  vpc_id = "${aws_vpc.default.id}"
  name = "teraform-test-http-sg"

  tags {
    Name = "${var.tag}-http-sg"
  }
}
resource "aws_security_group_rule" "http-inbound" {
  type = "ingress"
  security_group_id = "${aws_security_group.http.id}"
  from_port = 80
  to_port = 80
  protocol = "tcp"

  cidr_blocks = [
    "0.0.0.0/0"]
}
resource "aws_security_group_rule" "http-outbound" {
  type = "egress"
  security_group_id = "${aws_security_group.http.id}"
  from_port = 0
  to_port = 65535
  protocol = "tcp"

  cidr_blocks = [
    "0.0.0.0/0"]
}
resource "aws_security_group" "ssh" {
  description = "Terraform Test SSH SG"
  vpc_id = "${aws_vpc.default.id}"
  name = "teraform-test-ssh-sg"

  tags {
    Name = "${var.tag}-ssh-sg"
  }
}
resource "aws_security_group_rule" "ssh-inbound" {
  type = "ingress"
  security_group_id = "${aws_security_group.ssh.id}"
  from_port = 22
  to_port = 22
  protocol = "tcp"

  cidr_blocks = [
    "0.0.0.0/0"]
}
resource "aws_security_group_rule" "ssh-outbound" {
  type = "egress"
  security_group_id = "${aws_security_group.ssh.id}"
  from_port = 0
  to_port = 65535
  protocol = "tcp"

  cidr_blocks = [
    "0.0.0.0/0"]
}
resource "aws_security_group" "proxy" {
  description = "Terraform Test HTTP SG"
  vpc_id = "${aws_vpc.default.id}"
  name = "teraform-test-proxy-sg"

  tags {
    Name = "${var.tag}-proxy-sg"
  }
}
resource "aws_security_group_rule" "proxy-inbound" {
  type = "ingress"
  security_group_id = "${aws_security_group.proxy.id}"
  from_port = 0
  to_port = 65535
  protocol = "tcp"

  cidr_blocks = [
    "172.17.0.0/16"]
}
resource "aws_security_group_rule" "proxy-outbound" {
  type = "egress"
  security_group_id = "${aws_security_group.proxy.id}"
  from_port = 0
  to_port = 65535
  protocol = "tcp"

  cidr_blocks = [
    "0.0.0.0/0"]
}