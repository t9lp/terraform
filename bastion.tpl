#! /bin/bash

yum -y update
mkdir /root/.aws/
echo "[default]
region = ${region}" >> /root/.aws/config
aws ec2 associate-address --instance-id $(curl http://169.254.169.254/latest/meta-data/instance-id) --allocation-id ${aws_eip} --allow-reassociation