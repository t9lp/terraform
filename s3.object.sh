#! /bin/sh

SRC="site/"
TF_FILE="files.tf"
COUNT=0

> $TF_FILE

find $SRC -iname '*.*' | while read path; do

    cat >> $TF_FILE << EOM

resource "aws_s3_bucket_object" "file_$COUNT" {
  bucket = "\${aws_s3_bucket.default.id}"
  key = "${path#$SRC}"
  source = "$path"
  content_type = "\${lookup(var.mime_types, "${path##*.}")}"
  etag = "\${md5(file("$path"))}"
  acl = "public-read"
}
EOM

    COUNT=$(expr $COUNT + 1)

done