resource "aws_s3_bucket" "default" {
  bucket = "${var.bucket-name}"
  acl = "public-read"
  website {
        index_document = "index.html"
        error_document = "error.html"
    }
}