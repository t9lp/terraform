resource "aws_vpc" "default" {
  cidr_block = "${var.cidr_block}"
  enable_dns_support = true
  enable_dns_hostnames = true
  tags {
    Name = "${var.tag}-VPC"
  }
}
resource "aws_internet_gateway" "default" {
  vpc_id = "${aws_vpc.default.id}"
  tags {
    Name = "${var.tag}-internet-gateway"
  }
}
resource "aws_subnet" "default-public" {
  count = "${local.subnet_count}"
  vpc_id = "${aws_vpc.default.id}"
  availability_zone = "${element(local.availability_zones, count.index)}"
  cidr_block = "${var.cidr_half_block}${count.index + 1}.0/24"
  map_public_ip_on_launch = true

  tags {
    Name = "${element(local.availability_zones, count.index)}-public-vpc"
  }
}
resource "aws_subnet" "default-private" {
  count = "${local.subnet_count}"
  vpc_id = "${aws_vpc.default.id}"
  availability_zone = "${element(local.availability_zones, count.index)}"
  cidr_block = "${var.cidr_half_block}${count.index + 50}.0/24"
  map_public_ip_on_launch = false

  tags {
    Name = "${element(local.availability_zones, count.index)}-private-vpc"
  }
}
resource "aws_route_table" "internet_access" {
  vpc_id = "${aws_vpc.default.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.default.id}"
  }

  tags {
    Name = "${var.tag}-internet-route"
  }
}
resource "aws_route_table" "private_access" {
  count = "${local.subnet_count}"
  vpc_id = "${aws_vpc.default.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${element(aws_nat_gateway.default-nat.*.id, count.index)}"
  }

  tags {
    Name = "${var.tag}-private-route-${count.index}"
  }
}
resource "aws_route_table_association" "env_rt_table_association_public" {
  count = "${local.subnet_count}"
  subnet_id = "${element(aws_subnet.default-public.*.id, count.index)}"
  route_table_id = "${aws_route_table.internet_access.id}"
}
resource "aws_route_table_association" "env_rt_table_association_private" {
  count = "${local.subnet_count}"
  subnet_id = "${element(aws_subnet.default-private.*.id, count.index)}"
  route_table_id = "${element(aws_route_table.private_access.*.id, count.index)}"
}
resource "aws_eip" "nat_eip" {
  count = "${local.subnet_count}"
  vpc = true
  depends_on = [
    "aws_internet_gateway.default"]
}
resource "aws_nat_gateway" "default-nat" {
  count = "${local.subnet_count}"
  allocation_id = "${element(aws_eip.nat_eip.*.id, count.index)}"
  subnet_id = "${element(aws_subnet.default-public.*.id, count.index)}"

  tags {
    Name = "${var.tag}-nat-gw-${count.index}"
  }
}