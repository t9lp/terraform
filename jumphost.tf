data "template_file" "bastion" {
  template = "${file("bastion.tpl")}"
  vars {
    region = "${var.region}"
    aws_eip = "${aws_eip.bastion.id}"
  }
}
resource "aws_launch_configuration" "bastion" {
  name = "Bastion"
  image_id = "${var.aws-ami}"
  instance_type = "${var.instance_type}"
  associate_public_ip_address = true
  iam_instance_profile = "${aws_iam_instance_profile.bastion.name}"
  key_name = "${var.key_name}"
  security_groups = [
    "${aws_security_group.ssh.id }"]
  root_block_device {
    delete_on_termination = false
    volume_size = 50
    volume_type = "gp2"
  }
  user_data = "${data.template_file.bastion.rendered}"
}
resource "aws_eip" "bastion" {
  vpc = true
}
resource "aws_autoscaling_group" "bastion" {
  name = "Bastion"
  health_check_type = "EC2"
  launch_configuration = "${aws_launch_configuration.bastion.name}"
  max_size = 1
  min_size = 1
  vpc_zone_identifier = [
    "${aws_subnet.default-public.*.id}"]
}
output "bastion-public-ip" {
  value = "${aws_eip.bastion.public_ip}"
  description = "Bastion ip"
}