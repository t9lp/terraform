resource "aws_s3_bucket_object" "file_0" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "scss/_masthead.scss"
  source = "site/scss/_masthead.scss"
  content_type = "${lookup(var.mime_types, "scss")}"
  etag = "${md5(file("site/scss/_masthead.scss"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_1" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "scss/coming-soon.scss"
  source = "site/scss/coming-soon.scss"
  content_type = "${lookup(var.mime_types, "scss")}"
  etag = "${md5(file("site/scss/coming-soon.scss"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_2" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "scss/_mixins.scss"
  source = "site/scss/_mixins.scss"
  content_type = "${lookup(var.mime_types, "scss")}"
  etag = "${md5(file("site/scss/_mixins.scss"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_3" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "scss/_global.scss"
  source = "site/scss/_global.scss"
  content_type = "${lookup(var.mime_types, "scss")}"
  etag = "${md5(file("site/scss/_global.scss"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_4" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "scss/_bootstrap-overrides.scss"
  source = "site/scss/_bootstrap-overrides.scss"
  content_type = "${lookup(var.mime_types, "scss")}"
  etag = "${md5(file("site/scss/_bootstrap-overrides.scss"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_5" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "scss/_variables.scss"
  source = "site/scss/_variables.scss"
  content_type = "${lookup(var.mime_types, "scss")}"
  etag = "${md5(file("site/scss/_variables.scss"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_6" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "scss/_social.scss"
  source = "site/scss/_social.scss"
  content_type = "${lookup(var.mime_types, "scss")}"
  etag = "${md5(file("site/scss/_social.scss"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_7" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "gulpfile.js"
  source = "site/gulpfile.js"
  content_type = "${lookup(var.mime_types, "js")}"
  etag = "${md5(file("site/gulpfile.js"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_8" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "package-lock.json"
  source = "site/package-lock.json"
  content_type = "${lookup(var.mime_types, "json")}"
  etag = "${md5(file("site/package-lock.json"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_9" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "error.html"
  source = "site/error.html"
  content_type = "${lookup(var.mime_types, "html")}"
  etag = "${md5(file("site/error.html"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_10" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "img/bg-mobile-fallback.jpg"
  source = "site/img/bg-mobile-fallback.jpg"
  content_type = "${lookup(var.mime_types, "jpg")}"
  etag = "${md5(file("site/img/bg-mobile-fallback.jpg"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_11" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "package.json"
  source = "site/package.json"
  content_type = "${lookup(var.mime_types, "json")}"
  etag = "${md5(file("site/package.json"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_12" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/bootstrap/css/bootstrap-grid.min.css"
  source = "site/vendor/bootstrap/css/bootstrap-grid.min.css"
  content_type = "${lookup(var.mime_types, "css")}"
  etag = "${md5(file("site/vendor/bootstrap/css/bootstrap-grid.min.css"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_13" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/bootstrap/css/bootstrap-grid.min.css.map"
  source = "site/vendor/bootstrap/css/bootstrap-grid.min.css.map"
  content_type = "${lookup(var.mime_types, "map")}"
  etag = "${md5(file("site/vendor/bootstrap/css/bootstrap-grid.min.css.map"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_14" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/bootstrap/css/bootstrap-grid.css"
  source = "site/vendor/bootstrap/css/bootstrap-grid.css"
  content_type = "${lookup(var.mime_types, "css")}"
  etag = "${md5(file("site/vendor/bootstrap/css/bootstrap-grid.css"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_15" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/bootstrap/css/bootstrap-reboot.css"
  source = "site/vendor/bootstrap/css/bootstrap-reboot.css"
  content_type = "${lookup(var.mime_types, "css")}"
  etag = "${md5(file("site/vendor/bootstrap/css/bootstrap-reboot.css"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_16" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/bootstrap/css/bootstrap-reboot.min.css.map"
  source = "site/vendor/bootstrap/css/bootstrap-reboot.min.css.map"
  content_type = "${lookup(var.mime_types, "map")}"
  etag = "${md5(file("site/vendor/bootstrap/css/bootstrap-reboot.min.css.map"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_17" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/bootstrap/css/bootstrap.css"
  source = "site/vendor/bootstrap/css/bootstrap.css"
  content_type = "${lookup(var.mime_types, "css")}"
  etag = "${md5(file("site/vendor/bootstrap/css/bootstrap.css"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_18" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/bootstrap/css/bootstrap.min.css"
  source = "site/vendor/bootstrap/css/bootstrap.min.css"
  content_type = "${lookup(var.mime_types, "css")}"
  etag = "${md5(file("site/vendor/bootstrap/css/bootstrap.min.css"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_19" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/bootstrap/css/bootstrap.css.map"
  source = "site/vendor/bootstrap/css/bootstrap.css.map"
  content_type = "${lookup(var.mime_types, "map")}"
  etag = "${md5(file("site/vendor/bootstrap/css/bootstrap.css.map"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_20" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/bootstrap/css/bootstrap-grid.css.map"
  source = "site/vendor/bootstrap/css/bootstrap-grid.css.map"
  content_type = "${lookup(var.mime_types, "map")}"
  etag = "${md5(file("site/vendor/bootstrap/css/bootstrap-grid.css.map"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_21" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/bootstrap/css/bootstrap-reboot.min.css"
  source = "site/vendor/bootstrap/css/bootstrap-reboot.min.css"
  content_type = "${lookup(var.mime_types, "css")}"
  etag = "${md5(file("site/vendor/bootstrap/css/bootstrap-reboot.min.css"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_22" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/bootstrap/css/bootstrap-reboot.css.map"
  source = "site/vendor/bootstrap/css/bootstrap-reboot.css.map"
  content_type = "${lookup(var.mime_types, "map")}"
  etag = "${md5(file("site/vendor/bootstrap/css/bootstrap-reboot.css.map"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_23" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/bootstrap/css/bootstrap.min.css.map"
  source = "site/vendor/bootstrap/css/bootstrap.min.css.map"
  content_type = "${lookup(var.mime_types, "map")}"
  etag = "${md5(file("site/vendor/bootstrap/css/bootstrap.min.css.map"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_24" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/bootstrap/js/bootstrap.bundle.js.map"
  source = "site/vendor/bootstrap/js/bootstrap.bundle.js.map"
  content_type = "${lookup(var.mime_types, "map")}"
  etag = "${md5(file("site/vendor/bootstrap/js/bootstrap.bundle.js.map"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_25" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/bootstrap/js/bootstrap.min.js.map"
  source = "site/vendor/bootstrap/js/bootstrap.min.js.map"
  content_type = "${lookup(var.mime_types, "map")}"
  etag = "${md5(file("site/vendor/bootstrap/js/bootstrap.min.js.map"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_26" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/bootstrap/js/bootstrap.bundle.min.js"
  source = "site/vendor/bootstrap/js/bootstrap.bundle.min.js"
  content_type = "${lookup(var.mime_types, "js")}"
  etag = "${md5(file("site/vendor/bootstrap/js/bootstrap.bundle.min.js"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_27" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/bootstrap/js/bootstrap.js.map"
  source = "site/vendor/bootstrap/js/bootstrap.js.map"
  content_type = "${lookup(var.mime_types, "map")}"
  etag = "${md5(file("site/vendor/bootstrap/js/bootstrap.js.map"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_28" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/bootstrap/js/bootstrap.bundle.min.js.map"
  source = "site/vendor/bootstrap/js/bootstrap.bundle.min.js.map"
  content_type = "${lookup(var.mime_types, "map")}"
  etag = "${md5(file("site/vendor/bootstrap/js/bootstrap.bundle.min.js.map"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_29" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/bootstrap/js/bootstrap.bundle.js"
  source = "site/vendor/bootstrap/js/bootstrap.bundle.js"
  content_type = "${lookup(var.mime_types, "js")}"
  etag = "${md5(file("site/vendor/bootstrap/js/bootstrap.bundle.js"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_30" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/bootstrap/js/bootstrap.js"
  source = "site/vendor/bootstrap/js/bootstrap.js"
  content_type = "${lookup(var.mime_types, "js")}"
  etag = "${md5(file("site/vendor/bootstrap/js/bootstrap.js"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_31" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/bootstrap/js/bootstrap.min.js"
  source = "site/vendor/bootstrap/js/bootstrap.min.js"
  content_type = "${lookup(var.mime_types, "js")}"
  etag = "${md5(file("site/vendor/bootstrap/js/bootstrap.min.js"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_32" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/scss/_fixed-width.scss"
  source = "site/vendor/font-awesome/scss/_fixed-width.scss"
  content_type = "${lookup(var.mime_types, "scss")}"
  etag = "${md5(file("site/vendor/font-awesome/scss/_fixed-width.scss"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_33" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/scss/_icons.scss"
  source = "site/vendor/font-awesome/scss/_icons.scss"
  content_type = "${lookup(var.mime_types, "scss")}"
  etag = "${md5(file("site/vendor/font-awesome/scss/_icons.scss"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_34" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/scss/_rotated-flipped.scss"
  source = "site/vendor/font-awesome/scss/_rotated-flipped.scss"
  content_type = "${lookup(var.mime_types, "scss")}"
  etag = "${md5(file("site/vendor/font-awesome/scss/_rotated-flipped.scss"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_35" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/scss/font-awesome.scss"
  source = "site/vendor/font-awesome/scss/font-awesome.scss"
  content_type = "${lookup(var.mime_types, "scss")}"
  etag = "${md5(file("site/vendor/font-awesome/scss/font-awesome.scss"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_36" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/scss/_core.scss"
  source = "site/vendor/font-awesome/scss/_core.scss"
  content_type = "${lookup(var.mime_types, "scss")}"
  etag = "${md5(file("site/vendor/font-awesome/scss/_core.scss"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_37" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/scss/_mixins.scss"
  source = "site/vendor/font-awesome/scss/_mixins.scss"
  content_type = "${lookup(var.mime_types, "scss")}"
  etag = "${md5(file("site/vendor/font-awesome/scss/_mixins.scss"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_38" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/scss/_list.scss"
  source = "site/vendor/font-awesome/scss/_list.scss"
  content_type = "${lookup(var.mime_types, "scss")}"
  etag = "${md5(file("site/vendor/font-awesome/scss/_list.scss"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_39" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/scss/_animated.scss"
  source = "site/vendor/font-awesome/scss/_animated.scss"
  content_type = "${lookup(var.mime_types, "scss")}"
  etag = "${md5(file("site/vendor/font-awesome/scss/_animated.scss"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_40" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/scss/_bordered-pulled.scss"
  source = "site/vendor/font-awesome/scss/_bordered-pulled.scss"
  content_type = "${lookup(var.mime_types, "scss")}"
  etag = "${md5(file("site/vendor/font-awesome/scss/_bordered-pulled.scss"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_41" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/scss/_variables.scss"
  source = "site/vendor/font-awesome/scss/_variables.scss"
  content_type = "${lookup(var.mime_types, "scss")}"
  etag = "${md5(file("site/vendor/font-awesome/scss/_variables.scss"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_42" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/scss/_stacked.scss"
  source = "site/vendor/font-awesome/scss/_stacked.scss"
  content_type = "${lookup(var.mime_types, "scss")}"
  etag = "${md5(file("site/vendor/font-awesome/scss/_stacked.scss"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_43" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/scss/_larger.scss"
  source = "site/vendor/font-awesome/scss/_larger.scss"
  content_type = "${lookup(var.mime_types, "scss")}"
  etag = "${md5(file("site/vendor/font-awesome/scss/_larger.scss"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_44" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/scss/_screen-reader.scss"
  source = "site/vendor/font-awesome/scss/_screen-reader.scss"
  content_type = "${lookup(var.mime_types, "scss")}"
  etag = "${md5(file("site/vendor/font-awesome/scss/_screen-reader.scss"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_45" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/scss/_path.scss"
  source = "site/vendor/font-awesome/scss/_path.scss"
  content_type = "${lookup(var.mime_types, "scss")}"
  etag = "${md5(file("site/vendor/font-awesome/scss/_path.scss"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_46" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/less/stacked.less"
  source = "site/vendor/font-awesome/less/stacked.less"
  content_type = "${lookup(var.mime_types, "less")}"
  etag = "${md5(file("site/vendor/font-awesome/less/stacked.less"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_47" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/less/font-awesome.less"
  source = "site/vendor/font-awesome/less/font-awesome.less"
  content_type = "${lookup(var.mime_types, "less")}"
  etag = "${md5(file("site/vendor/font-awesome/less/font-awesome.less"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_48" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/less/bordered-pulled.less"
  source = "site/vendor/font-awesome/less/bordered-pulled.less"
  content_type = "${lookup(var.mime_types, "less")}"
  etag = "${md5(file("site/vendor/font-awesome/less/bordered-pulled.less"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_49" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/less/core.less"
  source = "site/vendor/font-awesome/less/core.less"
  content_type = "${lookup(var.mime_types, "less")}"
  etag = "${md5(file("site/vendor/font-awesome/less/core.less"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_50" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/less/screen-reader.less"
  source = "site/vendor/font-awesome/less/screen-reader.less"
  content_type = "${lookup(var.mime_types, "less")}"
  etag = "${md5(file("site/vendor/font-awesome/less/screen-reader.less"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_51" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/less/icons.less"
  source = "site/vendor/font-awesome/less/icons.less"
  content_type = "${lookup(var.mime_types, "less")}"
  etag = "${md5(file("site/vendor/font-awesome/less/icons.less"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_52" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/less/fixed-width.less"
  source = "site/vendor/font-awesome/less/fixed-width.less"
  content_type = "${lookup(var.mime_types, "less")}"
  etag = "${md5(file("site/vendor/font-awesome/less/fixed-width.less"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_53" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/less/animated.less"
  source = "site/vendor/font-awesome/less/animated.less"
  content_type = "${lookup(var.mime_types, "less")}"
  etag = "${md5(file("site/vendor/font-awesome/less/animated.less"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_54" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/less/mixins.less"
  source = "site/vendor/font-awesome/less/mixins.less"
  content_type = "${lookup(var.mime_types, "less")}"
  etag = "${md5(file("site/vendor/font-awesome/less/mixins.less"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_55" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/less/variables.less"
  source = "site/vendor/font-awesome/less/variables.less"
  content_type = "${lookup(var.mime_types, "less")}"
  etag = "${md5(file("site/vendor/font-awesome/less/variables.less"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_56" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/less/list.less"
  source = "site/vendor/font-awesome/less/list.less"
  content_type = "${lookup(var.mime_types, "less")}"
  etag = "${md5(file("site/vendor/font-awesome/less/list.less"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_57" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/less/path.less"
  source = "site/vendor/font-awesome/less/path.less"
  content_type = "${lookup(var.mime_types, "less")}"
  etag = "${md5(file("site/vendor/font-awesome/less/path.less"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_58" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/less/rotated-flipped.less"
  source = "site/vendor/font-awesome/less/rotated-flipped.less"
  content_type = "${lookup(var.mime_types, "less")}"
  etag = "${md5(file("site/vendor/font-awesome/less/rotated-flipped.less"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_59" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/less/larger.less"
  source = "site/vendor/font-awesome/less/larger.less"
  content_type = "${lookup(var.mime_types, "less")}"
  etag = "${md5(file("site/vendor/font-awesome/less/larger.less"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_60" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/fonts/FontAwesome.otf"
  source = "site/vendor/font-awesome/fonts/FontAwesome.otf"
  content_type = "${lookup(var.mime_types, "otf")}"
  etag = "${md5(file("site/vendor/font-awesome/fonts/FontAwesome.otf"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_61" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/fonts/fontawesome-webfont.eot"
  source = "site/vendor/font-awesome/fonts/fontawesome-webfont.eot"
  content_type = "${lookup(var.mime_types, "eot")}"
  etag = "${md5(file("site/vendor/font-awesome/fonts/fontawesome-webfont.eot"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_62" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/fonts/fontawesome-webfont.woff2"
  source = "site/vendor/font-awesome/fonts/fontawesome-webfont.woff2"
  content_type = "${lookup(var.mime_types, "woff2")}"
  etag = "${md5(file("site/vendor/font-awesome/fonts/fontawesome-webfont.woff2"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_63" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/fonts/fontawesome-webfont.ttf"
  source = "site/vendor/font-awesome/fonts/fontawesome-webfont.ttf"
  content_type = "${lookup(var.mime_types, "ttf")}"
  etag = "${md5(file("site/vendor/font-awesome/fonts/fontawesome-webfont.ttf"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_64" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/fonts/fontawesome-webfont.woff"
  source = "site/vendor/font-awesome/fonts/fontawesome-webfont.woff"
  content_type = "${lookup(var.mime_types, "woff")}"
  etag = "${md5(file("site/vendor/font-awesome/fonts/fontawesome-webfont.woff"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_65" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/fonts/fontawesome-webfont.svg"
  source = "site/vendor/font-awesome/fonts/fontawesome-webfont.svg"
  content_type = "${lookup(var.mime_types, "svg")}"
  etag = "${md5(file("site/vendor/font-awesome/fonts/fontawesome-webfont.svg"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_66" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/css/font-awesome.css"
  source = "site/vendor/font-awesome/css/font-awesome.css"
  content_type = "${lookup(var.mime_types, "css")}"
  etag = "${md5(file("site/vendor/font-awesome/css/font-awesome.css"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_67" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/css/font-awesome.min.css"
  source = "site/vendor/font-awesome/css/font-awesome.min.css"
  content_type = "${lookup(var.mime_types, "css")}"
  etag = "${md5(file("site/vendor/font-awesome/css/font-awesome.min.css"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_68" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/font-awesome/css/font-awesome.css.map"
  source = "site/vendor/font-awesome/css/font-awesome.css.map"
  content_type = "${lookup(var.mime_types, "map")}"
  etag = "${md5(file("site/vendor/font-awesome/css/font-awesome.css.map"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_69" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/jquery/jquery.js"
  source = "site/vendor/jquery/jquery.js"
  content_type = "${lookup(var.mime_types, "js")}"
  etag = "${md5(file("site/vendor/jquery/jquery.js"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_70" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/jquery/jquery.min.map"
  source = "site/vendor/jquery/jquery.min.map"
  content_type = "${lookup(var.mime_types, "map")}"
  etag = "${md5(file("site/vendor/jquery/jquery.min.map"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_71" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/jquery/jquery.min.js"
  source = "site/vendor/jquery/jquery.min.js"
  content_type = "${lookup(var.mime_types, "js")}"
  etag = "${md5(file("site/vendor/jquery/jquery.min.js"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_72" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/jquery/jquery.slim.min.js"
  source = "site/vendor/jquery/jquery.slim.min.js"
  content_type = "${lookup(var.mime_types, "js")}"
  etag = "${md5(file("site/vendor/jquery/jquery.slim.min.js"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_73" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/jquery/jquery.slim.min.map"
  source = "site/vendor/jquery/jquery.slim.min.map"
  content_type = "${lookup(var.mime_types, "map")}"
  etag = "${md5(file("site/vendor/jquery/jquery.slim.min.map"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_74" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "vendor/jquery/jquery.slim.js"
  source = "site/vendor/jquery/jquery.slim.js"
  content_type = "${lookup(var.mime_types, "js")}"
  etag = "${md5(file("site/vendor/jquery/jquery.slim.js"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_75" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "css/coming-soon.min.css"
  source = "site/css/coming-soon.min.css"
  content_type = "${lookup(var.mime_types, "css")}"
  etag = "${md5(file("site/css/coming-soon.min.css"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_76" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "css/coming-soon.css"
  source = "site/css/coming-soon.css"
  content_type = "${lookup(var.mime_types, "css")}"
  etag = "${md5(file("site/css/coming-soon.css"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_77" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "mp4/bg.mp4"
  source = "site/mp4/bg.mp4"
  content_type = "${lookup(var.mime_types, "mp4")}"
  etag = "${md5(file("site/mp4/bg.mp4"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_78" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "index.html"
  source = "site/index.html"
  content_type = "${lookup(var.mime_types, "html")}"
  etag = "${md5(file("site/index.html"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_79" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "js/coming-soon.min.js"
  source = "site/js/coming-soon.min.js"
  content_type = "${lookup(var.mime_types, "js")}"
  etag = "${md5(file("site/js/coming-soon.min.js"))}"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "file_80" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "js/coming-soon.js"
  source = "site/js/coming-soon.js"
  content_type = "${lookup(var.mime_types, "js")}"
  etag = "${md5(file("site/js/coming-soon.js"))}"
  acl = "public-read"
}
