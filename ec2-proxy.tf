data "template_file" "user_data" {
  template = "${file("userdata.tpl")}"
  vars {
    bucket = "${var.bucket-name}"
    region = "${var.region}"
  }

  depends_on = [
    "aws_nat_gateway.default-nat"]
}
resource "aws_launch_configuration" "http" {
  name = "${var.tag}-launch-template"
  image_id = "${var.aws-ami}"
  instance_type = "${var.instance_type}"
  key_name = "${var.key_name}"
  user_data = "${data.template_file.user_data.rendered}"
  security_groups = [
    "${aws_security_group.proxy.id}"]
}
resource "aws_autoscaling_policy" "http" {
  name = "${var.tag}-autoscaling-policy"
  adjustment_type = "ChangeInCapacity"
  policy_type = "TargetTrackingScaling"
  autoscaling_group_name = "${aws_autoscaling_group.http.name}"
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
    target_value = 80.0
  }
}
resource "aws_autoscaling_group" "http" {
  name = "${var.tag}-autoscaling-group"
  desired_capacity = 2
  max_size = 5
  min_size = 2
  health_check_type = "EC2"
  target_group_arns = [
    "${aws_lb_target_group.default.arn}"]
  launch_configuration = "${aws_launch_configuration.http.id}"
  vpc_zone_identifier = [
    "${aws_subnet.default-private.*.id}"]
}