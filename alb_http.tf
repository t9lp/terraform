resource "aws_lb" "default" {
  name = "${var.tag}-http-alb"
  internal = false
  load_balancer_type = "application"
  security_groups = [
    "${aws_security_group.http.id}"]
  subnets = [
    "${aws_subnet.default-public.*.id}"]
  ip_address_type = "ipv4"
  enable_deletion_protection = false

  tags {
    Environment = "${var.tag}-http-alb"
  }
}
resource "aws_lb_target_group" "default" {
  name = "${var.tag}-http-target-group"
  vpc_id = "${aws_vpc.default.id}"
  port = "8080"
  protocol = "HTTP"
  deregistration_delay = "300"
  target_type = "instance"

  health_check {
    interval = "10"
    path = "/"
    healthy_threshold = "3"
    unhealthy_threshold = "3"
    timeout = "6"
    protocol = "HTTP"
    matcher = "200-299"
  }
}
resource "aws_lb_listener" "frontend_http_tcp_no_logs" {
  load_balancer_arn = "${aws_lb.default.arn}"
  port = "80"
  protocol = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.default.arn}"
    type = "forward"
  }
}
output "dns_name" {
  value = "${aws_lb.default.dns_name}"
  description = "Site dns_name."
}